#!/bin/bash
dotnet restore

dotnet tool install --global dotnet-ef
dotnet ef --project src/EFGetStarted database update
