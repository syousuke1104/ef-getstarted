﻿using EFGetStarted;
using EFGetStarted.Models;

using var db = new BloggingContext();

// Note: This sample requires the database to be created before running.
Console.WriteLine($"Database path: {db.DbPath}.");

// Create
Console.WriteLine("ブログを作成.");
db.Add(new Blog { Url = "http://blogs.msdn.com/adonet" });
db.SaveChanges();

// Read
Console.WriteLine("ブログを検索.");
var blog = db.Blogs
    .OrderBy(b => b.BlogId)
    .First();

Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(blog, options: new() { WriteIndented = true }));

// Update
Console.WriteLine("ブログを更新.");
blog.Url = "https://devblogs.microsoft.com/dotnet";
blog.Posts.Add(
    new Post { Title = "Hello World", Content = "I wrote an app using EF Core!" });
db.SaveChanges();

// Delete
Console.WriteLine("ブログを削除.");
db.Remove(blog);
db.SaveChanges();
